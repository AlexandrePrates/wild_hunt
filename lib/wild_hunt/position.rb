module WildHunt
  class Position
    class << self
      def [](x, y)
        self.new x, y
      end
    end

    attr_reader :x, :y

    def initialize(x, y)
      @x = x
      @y = y
    end

    def distance_to(position)
      (self.x - position.x).abs + (self.y - position.y).abs
    end

    def x_distance(position)
      position.x - self.x
    end

    def y_distance(position)
      position.y - self.y
    end

    def inc(axis, amount = 1)
      case axis
      when 'x', :x
        @x += amount
      when 'y', :y
        @y += amount
      else
        fail ArgumentError, "invalid axis"
      end
    end

    def dec(axis, amount = 1)
      inc axis, (amount * -1)
    end

  end
end
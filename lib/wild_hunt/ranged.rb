module WildHunt
  class Ranged
    class << self
      def [](max)
        self.new(0, max)
      end
    end

    attr_reader :current, :min, :max

    def initialize(min, max)
      @min = min
      @max = max
      @current = max
    end

    def inc(amount = 1)
      @current += amount
      if self > max
        @current = max
        return false
      end
      @current
    end

    def dec(amount = 1)
      @current -= amount
      if self < min
        @current = min
        return false
      end
      @current
    end

    def force!(value)
      @current = value
      self
    end

    def full?
      current == max
    end

    def half?
      current <= fractionate_by(2)
    end

    def third?
      current <= fractionate_by(3)
    end

    def quarter?
      current <= fractionate_by(4)
    end

    def fifth?
      current <= fractionate_by(5)
    end

    def empty?
      current == min
    end

    %w{> >= < <= ==}.each do |operator|
      define_method(operator) { |value| self.current.send(operator, value) }
    end

    private

    def fractionate_by(value)
      max / value
    end

  end
end
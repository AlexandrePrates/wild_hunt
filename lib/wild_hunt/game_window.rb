module WildHunt
  class GameWindow < Gosu::Window

    def initialize
      super 1050, 800, false
      self.caption = "Wild Hunt"

      @background_image = Gosu::Image.new("./assets/backgrounds/grass_tile.png")
      @cow_image = Gosu::Image::load_tiles("./assets/sprites/cow.png", 35, 40)

      @cow = Cow.new(self, @cow_image)
    end

    def update
      @cow.update
    end

    def draw
      background_fill
      @cow.draw
    end

    private

     def background_fill
      (0..self.width/@background_image.width).each do |x|
        (0..self.height/@background_image.height).each do |y|
          @background_image.draw(
            x * (@background_image.width),
            y * (@background_image.height),
            0
          )
        end
      end
     end
  end
end

module WildHunt
  class Cattle
    STATES = [:standing, :walking, :eating, :running]

    attr_reader :location, :state, :stamina

    def initialize(location, stamina)
      @location = location
      @stamina = stamina
      @base_stamina_loss = stamina.max / 10
      standing!
    end

    STATES.each do |state|
      define_method("#{state}!") { @state = state; self }
      define_method("#{state}?") { @state == state }
    end

    def act
      eating! if stamina.fifth?
      send "on_#{state}"
    end

    private

    def base_stamina_loss(factor = 1)
      @base_stamina_loss * factor
    end

    # State machine triggers

    def on_standing
      stamina.dec base_stamina_loss
    end

    def on_walking
      stamina.dec base_stamina_loss(2)
    end

    def on_running
      stamina.dec base_stamina_loss(3)
    end

    def on_eating
      stamina.inc base_stamina_loss(4)
      walking! if stamina.full?
    end

  end
end

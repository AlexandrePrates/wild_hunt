module WildHunt
  module Helpers
    class Sprite
      attr_reader :action

      def initialize(sprite)
        @sprite =         sprite
        @current_action = ''
        @action =         {}
      end

      def register_action(name, params)
        @action[name] = {
          images: @sprite[params[:initial]..params[:final]],
          frame_hate: params[:frame_hate]
        }
      end

      def execute_action(name)
        if name == @current_action
          @last_time ||= Gosu.milliseconds
          @index     ||= 0

          next_image(name) if Gosu.milliseconds - @last_time > @action[name][:frame_hate]
        else
          @current_image = @action[name][:images][0]
          @index = 0
          @current_action = name
        end
      end

      def draw(x, y, z)
        @current_image.draw(x, y, z, 1)
      end

      private

      def next_image(name)
        @index = 0 if @action[name][:images][@index].nil?
        @current_image = @action[name][:images][@index]
        @index += 1
        @last_time = Gosu.milliseconds
      end
    end
  end
end

module WildHunt
  class Cow
    def initialize(window, image)
      @window = window
      @sprite = WildHunt::Helpers::Sprite.new(image)
      @event_time = 1000

      set_actions
    end

    def draw
      @sprite.draw(@window.width/2, @window.height/2, 0)
    end

    def update
      @sprite.execute_action(:move_left)
    end

    private

    def set_actions
      @sprite.register_action(:move_left, { initial: 1, final: 2, frame_hate: 150 })
      @sprite.register_action(:crazy, { initial: 30, final: 31, frame_hate: 250 })
    end
  end
end

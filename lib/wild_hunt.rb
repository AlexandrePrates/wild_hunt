require 'bundler'
Bundler.require

require './lib/wild_hunt/helpers/sprite'
require './lib/wild_hunt/ranged'
require './lib/wild_hunt/position'
require './lib/wild_hunt/cattle'
require './lib/wild_hunt/cow'
require './lib/wild_hunt/game_window'

module WildHunt
  extend self

  def run
    GameWindow.new.show
  end
end

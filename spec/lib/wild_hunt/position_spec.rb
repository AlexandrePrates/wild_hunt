require 'spec_helper'

describe WildHunt::Position do
  describe ".[]" do
    it "create new position with values" do
      position = described_class[10,10]
      expect(position).to be_kind_of(described_class)
      expect(position.x).to eq 10
      expect(position.y).to eq 10
    end
  end

  let(:position) { described_class.new 1, 1 }
  let(:other_position) { described_class.new 4, 5 }

  describe ".new" do
    it "set x and y" do
      expect(position.x).to eq 1
      expect(position.y).to eq 1
    end
  end

  describe "#distance_to" do
    it "return distance between two positions" do
      expect(position.distance_to(other_position)).to eq 7
    end

    it "distance is the same each other" do
      expect(position.distance_to(other_position)).to eq other_position.distance_to(position)
    end
  end

  describe "#x_distance" do
    it "axix x distance between two positions" do
      expect(position.x_distance(other_position)).to eq 3
    end

    it "signal changes with referential" do
      expect(other_position.x_distance(position)).to eq -3
    end
  end

  describe "#y_distance" do
    it "axix y distance between two positions" do
      expect(position.y_distance(other_position)).to eq 4
    end

    it "signal changes with referential" do
      expect(other_position.y_distance(position)).to eq -4
    end
  end

  describe "#inc" do
    it "increment x axis" do
      position.inc :x
      expect(position.x).to eq 2
    end

    it "increment y axis" do
      position.inc :y
      expect(position.y).to eq 2
    end

    it "increment by value" do
      position.inc :x, 5
      expect(position.x).to eq 6
    end

    it "raise error on invalid axis" do
      expect { position.inc :z, 10 }.to raise_error ArgumentError
    end
  end

  describe "#dec" do
    it "decrement x axis" do
      position.dec :x
      expect(position.x).to eq 0
    end

    it "decrement y axis" do
      position.dec :y
      expect(position.y).to eq 0
    end

    it "decrement by value" do
      position.dec :x, 5
      expect(position.x).to eq -4
    end
  end
end

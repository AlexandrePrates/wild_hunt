require 'spec_helper'

describe WildHunt::Ranged do
  let(:max_value) { 20 }
  let(:ranged) { described_class[20] }

  describe ".[](value)" do
    it "create instance with 0 as minimal" do
      expect(ranged.min).to eq 0
    end

    it "create instance with value as maximum" do
      expect(ranged.max).to eq max_value
    end
  end

  describe ".new" do
    it "current is equal maximum" do
      expect(ranged.current).to eq ranged.max
    end
  end

  describe "#inc" do
    it "current can't be greater then max" do
      expect(ranged).to eq max_value
      ranged.inc
      expect(ranged).to eq max_value
    end

    it "increment current" do
      ranged.force!(5)
      expect(ranged.inc).to eq 6
    end

    it "return false when reach max" do
      expect(ranged.inc).to be false
      expect(ranged).to eq max_value
    end
  end

  describe "#dec" do
    it "current can't be smaller then min" do
      ranged.force!(0)
      ranged.dec
      expect(ranged).to eq 0
    end

    it "decrement current" do
      expect(ranged.dec).to eq (max_value - 1)
    end

    it "return nil when min is reached" do
      ranged.force!(0)
      expect(ranged.dec).to be_falsey
    end
  end

  describe "operators" do
    it "true operation" do
      expect(ranged < 100).to be true
    end
    it "false operation" do
      expect(ranged > 100).to be false
    end
  end

  describe "#force!" do
    it "force current as value" do
      expect(ranged).to eq max_value
      ranged.force!(3)
      expect(ranged).to eq 3
    end
  end

  describe "full?" do
    it "true when current is equal full" do
      expect(ranged).to be_full
    end
    it "false when current is not equal full" do
      ranged.dec
      expect(ranged).not_to be_full
    end
  end

  describe "#half?" do
    it "return true when the current is less than half" do
      half_part = max_value / 2
      ranged.force! half_part + 1
      expect(ranged).not_to be_half
      ranged.force! half_part
      expect(ranged).to be_half
    end
  end

  describe "#third?" do
    it "return true when the current is less than third" do
      third_part = max_value / 3
      ranged.force! third_part + 1
      expect(ranged).not_to be_third
      ranged.force! third_part
      expect(ranged).to be_third
    end
  end

  describe "#quarter?" do
    it "return true when the current is less than quarter" do
      quarter_part = max_value / 4
      ranged.force! quarter_part + 1
      expect(ranged).not_to be_quarter
      ranged.force! quarter_part
      expect(ranged).to be_quarter
    end
  end

  describe "#fifth?" do
    it "return true when the current is less than fifth" do
      fifth_part = max_value / 5
      ranged.force! fifth_part + 1
      expect(ranged).not_to be_fifth
      ranged.force! fifth_part
      expect(ranged).to be_fifth
    end
  end

  describe "empty?" do
    it "true when current is equal empty" do
      ranged.force!(0)
      expect(ranged).to be_empty
    end
    it "false when current is not equal empty" do
      ranged.force!(5)
      expect(ranged).not_to be_empty
    end
  end

end

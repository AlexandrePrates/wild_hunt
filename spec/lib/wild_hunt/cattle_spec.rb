require 'spec_helper'

describe WildHunt::Cattle do
  let(:cow) { described_class.new WildHunt::Position[10, 10], WildHunt::Ranged[20] }

  describe "#location" do
    it "be on spawned location" do
      expect(cow.location.x).to eq 10
      expect(cow.location.x).to eq 10
    end
  end

  describe "#state" do
    it "spawn as standing" do
      expect(cow.state).to be :standing
    end

    it "has custom setters" do
      expect(cow.walking!.state).to be :walking
    end

    it "setter change current object" do
      some_cow = cow
      expect(some_cow.state).to be :standing
      cow.walking!
      expect(some_cow.state).to be :walking
    end

    it "has custom questers" do
      expect(cow.standing?).to be true
      cow.walking!
      expect(cow.walking?).to be true
    end
  end

  describe "#stamina" do
    it "spawn as max stamina" do
      expect(cow.stamina).to eq cow.stamina.max
    end

    it "decrease by 1 when standing" do
      cow.send :on_standing
      expect(cow.stamina).to eq 18
    end

    it "decrease by 2 when walking" do
      cow.send :on_walking
      expect(cow.stamina).to eq 16
    end

    it "decrease by 3 when running" do
      cow.send :on_running
      expect(cow.stamina).to eq 14
    end

    it "increase by 4 when eating" do
      cow.send :on_running
      cow.send :on_walking
      cow.send :on_eating
      expect(cow.stamina).to eq 18
      cow.send :on_eating
      expect(cow.stamina).to eq cow.stamina.max
    end
  end

  describe "#act" do
    it "changes to walking after eating" do
      cow.eating!
      cow.stamina.force!(3)
      cow.act
      expect(cow).to be_eating
      cow.act
      expect(cow).to be_eating
      cow.act
      expect(cow.stamina).to be_full
      expect(cow).to be_walking
    end

    it "need eat when stamina drop to fifth" do
      cow.standing!
      cow.stamina.force!(1)
      cow.act
      expect(cow).to be_eating
      cow.act
      expect(cow).to be_eating
      cow.act
      expect(cow).to be_walking
    end
  end

end
